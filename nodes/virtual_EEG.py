#!/usr/bin/env python


import rospy
from std_msgs.msg import Float64
import numpy as np

def talker():
    pub = rospy.Publisher("EEG_signal", Float64, queue_size=1)
    rospy.init_node('virtual_eeg', anonymous=True)
    rate = rospy.Rate(10) # 10hz
    eeg_state = 0
    windup = 1
    reduction = 0.1
    while not rospy.is_shutdown():
        eeg = np.random.randn()
        eeg_state += eeg*reduction
        if eeg_state>windup:
            eeg_state=windup
        if eeg_state<(-windup):
            eeg_state=-windup
        print("EEG Global State: "+str(eeg_state))
        pub.publish(eeg)
        rate.sleep()

if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass