#!/usr/bin/env python

# 
# We will scout the 6 dimensions in the Cartesian space so that we get for each discretized point if its feasible or not
    # for the non feasible ones we will include a gradient value that indicates where are the closest feasible points

    # The best way would be to mark initially as 0 the feasible points and as 1 the nonfeasible
    # can we use a blur to extend this ??
import rospy
import numpy as np
import copy
import transforms3d 
import struct
import time
import math

from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt

from geometry_msgs.msg import (
    PoseStamped,
    Pose,
    Point,
    Quaternion,
)
from std_msgs.msg import (
    Header, 
    String,
    Float64
)
from baxter_core_msgs.srv import (
    SolvePositionIK,
    SolvePositionIKRequest,
)

# limits --> based roughly on the baxter limits from the web (http://sdk.rethinkrobotics.com/wiki/Workspace_Guidelines)
xl = [-0.5, 1.5]
yl = [-1.5, 1,5]
zl = [-0.5, 1.5]
al = [0, (350*np.pi)/180]
bl = [0, (350*np.pi)/180]
gl = [0, (350*np.pi)/180]
# number of elements per dimension
xn = 81
yn = 101
zn = 81
an = 36 # 10 between points 
bn = 36
gn = 36 
#step size between points
xst = (xl[1]-xl[0])/(xn-1)
yst = (xl[1]-xl[0])/(xn-1)
zst = (xl[1]-xl[0])/(xn-1)
ast = (xl[1]-xl[0])/(xn-1)
bst = (xl[1]-xl[0])/(xn-1)
zst = (xl[1]-xl[0])/(xn-1)

total_points = xn*yn*zn*an*bn*gn
total_points_simplified = xn*yn*zn

step = [((xl[1]-xl[0])/(xn-1)), ((yl[1]-yl[0])/(yn-1)), ((zl[1]-zl[0])/(zn-1))]


def ik_conversion(gp_msg,iksvc,limb="right"):
    ns = "ExternalTools/"+ limb + "/PositionKinematicsNode/IKService"
    ikreq = SolvePositionIKRequest()
    ikreq.pose_stamp.append(gp_msg)
    try:
        rospy.wait_for_service(ns, 5.0)
        resp = iksvc(ikreq)
    except (rospy.ServiceException, rospy.ROSException), e:
        rospy.logerr("Service call failed: %s" % (e,))
        return False

    # Check if result valid, and type of seed ultimately used to get solution
    # convert rospy's string representation of uint8[]'s to int's
    resp_seeds = struct.unpack('<%dB' % len(resp.result_type),
                               resp.result_type)
    if (resp_seeds[0] != resp.RESULT_INVALID):
        return 0
    else:
        return 1


#here we need to do a 6 for loop to go through the whole cartesian space and find the feasible points (0) and the unfeasible(1)
def first_iteration():

    global xl,yl,zl,al,bl,gl,xn,yn,zn,an,bn,gn, total_points
    print("Total number of points: " + str(total_points))
    print("FIRST ITERATION: IK data gathering")

    mat = np.zeros((xn,yn,zn,an,bn,gn))
    next_goal=[0,0,0,0,0,0]
    next_pose=PoseStamped()
    i=0.0

    ns = "ExternalTools/"+ "right" + "/PositionKinematicsNode/IKService"
    iksvc = rospy.ServiceProxy(ns,SolvePositionIK)
    count=0
    # each dimension is discretized with the apropiate units:
        # value = ((x[1]-x[0])/xn)*linspace + x[0]
    for x in np.linspace(0,xn-1,xn):
        for y in np.linspace(0,yn-1,yn):
            for z in np.linspace(0,zn-1,zn):
                for a in np.linspace(0,an-1,an):
                    text = "Completed: "+str(i*100.0/total_points)+"%"
                    print(text)
                    for b in np.linspace(0,bn-1,bn):
                        for g in np.linspace(0,gn-1,gn):
                            i += 1
                            next_goal[0]=((xl[1]-xl[0])/(xn-1))*x + xl[0]
                            next_goal[1]=((yl[1]-yl[0])/(yn-1))*y + yl[0]
                            next_goal[2]=((zl[1]-zl[0])/(zn-1))*z + zl[0]
                            next_goal[3]=((al[1]-al[0])/(an-1))*a + al[0]
                            next_goal[4]=((bl[1]-bl[0])/(bn-1))*b + bl[0]
                            next_goal[5]=((gl[1]-gl[0])/(gn-1))*g + gl[0]

                            quat = transforms3d.euler.euler2quat(next_goal[3],next_goal[4],next_goal[5]) 
                            text = "point for feasibility check #"+str(i)
                            hdr = Header(stamp=rospy.Time.now(), frame_id='base')

                            next_pose.header = hdr
                            next_pose.pose.position.x = next_goal[0]
                            next_pose.pose.position.y = next_goal[1]
                            next_pose.pose.position.z = next_goal[2]
                            next_pose.pose.orientation.x = quat[1]
                            next_pose.pose.orientation.y = quat[2]
                            next_pose.pose.orientation.z = quat[3]
                            next_pose.pose.orientation.w = quat[0]

                            mat[x,y,z,a,b,g] = ik_conversion(next_pose,iksvc)
                            if mat[x,y,z,a,b,g] == 0:
                                count += 1
                                txt = "Number of feasible positions ==> " + str(count)
                                print(txt)

                            # print(("Completed: "+str(i*100/total_points)+"%"))
    return mat

def first_iteration_simplified():
    
    global xl,yl,zl,al,bl,gl,xn,yn,zn,an,bn,gn, total_points_simplified
    print("Total number of points: " + str(total_points_simplified))
    print("FIRST ITERATION: IK data gathering")
    mat = np.zeros((xn,yn,zn))
    next_goal=[0,0,0]
    next_pose=PoseStamped()
    i=0.0

    ns = "ExternalTools/"+ "right" + "/PositionKinematicsNode/IKService"
    iksvc = rospy.ServiceProxy(ns,SolvePositionIK)
    count=0

    quat = [0.261868353356,0.367048116303,0.885911751787,-0.108908281936]

    # each dimension is discretized with the apropiate units:
        # value = ((x[1]-x[0])/xn)*linspace + x[0]
    for x in np.linspace(0,xn-1,xn):
        text = "Completed: "+str(i*100.0/total_points_simplified)+"%"
        print(text)
        for y in np.linspace(0,yn-1,yn):
            for z in np.linspace(0,zn-1,zn):
                
                i += 1
                

                next_goal[0]=((xl[1]-xl[0])/(xn-1))*x + xl[0]
                next_goal[1]=((yl[1]-yl[0])/(yn-1))*y + yl[0]
                next_goal[2]=((zl[1]-zl[0])/(zn-1))*z + zl[0]
                
                text = "point for feasibility check #"+str(i)
                hdr = Header(stamp=rospy.Time.now(), frame_id='base')

                next_pose.header = hdr
                next_pose.pose.position.x = next_goal[0]
                next_pose.pose.position.y = next_goal[1]
                next_pose.pose.position.z = next_goal[2]
                next_pose.pose.orientation.x = quat[1]
                next_pose.pose.orientation.y = quat[2]
                next_pose.pose.orientation.z = quat[3]
                next_pose.pose.orientation.w = quat[0]

                mat[x,y,z] = ik_conversion(next_pose,iksvc)
                if mat[x,y,z] == 0:
                    count += 1
        txt = "Number of feasible positions ==> " + str(count)
        print(txt)

                # print(("Completed: "+str(i*100/total_points)+"%"))
    return mat

def get_closest(c,ze):
# case of a draw in distance we will select the one that came first --> CAN WE IMPROVE THIS ????? --> Which criteria would make more sense
    mi = 10000
    for p in ze:
        dist = np.sqrt(((c[0]-p[0])**2+(c[1]-p[1])**2+(c[2]-p[2])**2))
        if dist<mi:
            mi = dist
            closest = p
    return closest


def second_iteration(mat):
    # In each position we will store the coordinates (6D) of the nearest point with a 0
    # We will have a general loop with a while where we go through the whole space and we only fill gradients in case we have a 0 in the nearest neighbour 
        # Case of several 0: the one that has smaller distance in m --> then if again is a draw we will do it by random
    print("SECOND ITERATION: gradient information computation")
    global xl,yl,zl,al,bl,gl,xn,yn,zn,an,bn,gn, total_points
    gradients = np.zeros((xn,yn,zn,an,bn,gn,6))
    ze = []
    tw = []
    n=0
    m=0
    i=-1
    finish = False
    first_round = -1
    statistic = 0

    while finish == False:
        finish = True
        if first_round == 0:
            txt = "The percentage of unfeasible points after FI is: "+ str(statistic*100/total_points)+"%"
            print(txt)
        first_round += 1
        # we avoid the contours completely --> they will remain as [0,0,0,0,0,0]
        for x in np.linspace(1,xn-2,xn-2):
            for y in np.linspace(1,yn-2,yn-2):
                for z in np.linspace(1,zn-2,zn-2):
                    text = "Completed: "+str(i*100/total_points)+"%"
                    print(text)
                    for a in np.linspace(1,an-2,an-2):
                        for b in np.linspace(1,bn-2,bn-2):
                            for g in np.linspace(1,gn-2,gn-2):
                                i += 1
                                if mat[x,y,z,a,b,g]==1:
                                    if first_round<1:
                                        statistic += 1
                                    #check the 64 nearby positions: 1 loop of 64 converted to binary values ?
                                    finish = False
                                    for j in range(64):
                                        # Repair using the simplified solution

                                    if n>0:
                                        if n>1:
                                            gradients[x,y,z,a,b,g,:]=get_closest([x,y,z,a,b,g],ze)
                                            mat[x,y,z,a,b,g] = 2
                                        else:
                                            gradients[x,y,z,a,b,g,:]=ze[0]
                                            mat[x,y,z,a,b,g] = 2
                                    elif m>0:
                                        if m>1:
                                            gradients[x,y,z,a,b,g,:]=get_closest([x,y,z,a,b,g],tw)
                                            mat[x,y,z,a,b,g] = 2
                                        else:
                                            gradients[x,y,z,a,b,g,:]=tw[0]
                                            mat[x,y,z,a,b,g] = 2
                                    n = 0
                                    ze = []
                                    m=0
                                    tw = []
                                    # Sprint(("Completed: "+str(i*100/total_points)+"%"))


    return gradients

def second_iteration_simplified(mat):
    # In each position we will store the coordinates (3D) of the nearest point with a 0
    # We will have a general loop with a while where we go through the whole space and we only fill gradients in case we have a 0 in the nearest neighbour 
        # Case of several 0: the one that has smaller distance in m --> then if again is a draw we will do it by random
    print("SECOND ITERATION: gradient information computation")
    global xl,yl,zl,al,bl,gl,xn,yn,zn,an,bn,gn, total_points_simplified
    gradients = np.zeros((xn,yn,zn,3))
    ze = []
    tw = []
    n=0
    m=0
    finish = False
    first_round = -1
    statistic = 0
    i=0.0

    while finish == False:
        
        finish = True
        if first_round == 0:
            stat_zero = statistic
            txt = "The percentage of unfeasible points after FI is: "+ str(statistic*100/total_points_simplified)+"%"
            print(txt) 
            print(("   --> Time Zero # of points to explore in MAT is: " + str(statistic)))
            print(("   --> Total # of points in MAT is: " + str(total_points_simplified)))
        first_round += 1

        print("----- ROUND #"+str(first_round)+" -----")
        if first_round > 0:
            txt = "   --> Points to explore: "+ str((statistic)*100/stat_zero)+"%"
            print(txt)

        neig = [-1,0,1]

        i=0
        statistic=0
        # we avoid the contours completely --> they will remain as [0,0,0]
        for x in np.linspace(1,xn-2,xn-2):
            #text = "    Completed "+str(i*100.0/total_points_simplified)+"%"
            #print(text)
            for y in np.linspace(1,yn-2,yn-2):
                for z in np.linspace(1,zn-2,zn-2):
                    
                    i += 1
                    

                    if mat[x,y,z]==1:
                        statistic += 1
                        #check the 8 nearby positions: 1 loop of 8 converted to binary values ?
                        finish = False
                        for x_ne in neig:
                            for y_ne in neig:
                                for z_ne in neig:
                                    if mat[x+x_ne,y+y_ne,z+z_ne]==0:
                                        n+=1
                                        ze.append([x+x_ne,y+y_ne,z+z_ne])
                                    elif mat[x+x_ne,y+y_ne,z+z_ne]==2:
                                        m+=1
                                        tw.append([x+x_ne,y+y_ne,z+z_ne])
                        if n>0:
                            if n>1:
                                gradients[x,y,z,:]=get_closest([x,y,z],ze)
                                mat[x,y,z] = 2
                            else:
                                gradients[x,y,z,:]=ze[0]
                                mat[x,y,z] = 2
                        elif m>0:
                            if m>1:
                                gradients[x,y,z,:]=get_closest([x,y,z],tw)
                                mat[x,y,z] = 2
                            else:
                                gradients[x,y,z,:]=tw[0]
                                mat[x,y,z] = 2
                        n = 0
                        ze = []
                        m=0
                        tw = []
                                    # Sprint(("Completed: "+str(i*100/total_points)+"%"))


    return gradients


################################################################
#Import Functions: Matrix translation into useful values
    # We only implement for the 3DoF case

#We will recieve a 6 number list and a matrix version (original, simplified_1, simplified_2 ...) 
def get_index(point,version = 2):
    global step, xl, yl, zl

    x_sup = math.ceil(((point[0]-xl[0])/step[0]))
    x_inf = math.floor(((point[0]-xl[0])/step[0]))
    x_round = round(((point[0]-xl[0])/step[0]))
    y_sup = math.ceil(((point[1]-yl[0])/step[1]))
    y_inf = math.floor(((point[1]-yl[0])/step[1]))
    y_round = round(((point[1]-yl[0])/step[1]))
    z_sup = math.ceil(((point[2]-zl[0])/step[2]))
    z_inf = math.floor(((point[2]-zl[0])/step[2]))
    z_round = round(((point[2]-zl[0])/step[2]))


    return [[x_sup,x_inf],[y_sup,y_inf],[z_sup,z_inf],[x_round,y_round,z_round]]

def check_point(point,version = 2,mat):

    if version==1:
    #6DoF

    elif version==2:
    #3DoF --> orientation fixed with demo quaternion
    # Only if 8/8 points are valid we take the point as valid itself   ---------------->    WE SHOULD GET MORE EVEN IF IT "SEEMS" RIGHT (might be surrounded but is still not feasible)
        ind = get_index(point,version) # we need to go in every combinationn (2**3)
        p = [0,0,0]
        star = -1 
        valid = True
        close_solution = False
        for i in range(8):
            bin = np.binary_repr(i, width=3)
            if mat[ind[0][int(bin[0])],ind[1][int(bin[1])],ind[2][int(bin[2])]] == 1
                valid = False
            else:
                #we select the point that is closest to the perfect aproximation obtained in get_index
                if ind[0][int(bin[0])] == ind[3][0] 
                    p[0] = 1
                if ind[1][int(bin[1])] == ind[3][1]
                    p[1] = 1
                if ind[2][int(bin[2])] == ind[3][2]
                    p[2] = 1

                rookie = p[0] + p[1] + p[2]
                p = [0,0,0]
                if rookie>star
                    close_solution = True
                    star = rookie
                    close_right = [ind[0][int(bin[0])],ind[1][int(bin[1])],ind[2][int(bin[2])]]
                    
        return [valid,close_solution,close_right]
                        

    else:
        print("This Version has not been implemented")



################################################################
#Visualization

def feasible_plot(mat):
    global xl,yl,zl,al,bl,gl,xn,yn,zn,an,bn,gn, total_points_simplified
    x_list = [0]
    y_list = [0]
    z_list = [0]
    size = [30]
    colour = ['g']

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    i=0.0
    print("3D VISUALIZATION: feasible points representation")
    for x in np.linspace(0,xn-1,xn):
        text = "Completed: "+str(i*100.0/total_points_simplified)+"%"
        print(text)
        for y in np.linspace(0,yn-1,yn):
            for z in np.linspace(0,zn-1,zn):
                i += 1

                if mat[x,y,z]==0:
                    x_list.append(((xl[1]-xl[0])/xn)*x + xl[0])
                    y_list.append(((yl[1]-yl[0])/yn)*y + yl[0])
                    z_list.append(((zl[1]-zl[0])/zn)*z + zl[0])
                    size.append(10)
                    colour.append('b')

    ax.scatter(np.array(x_list), np.array(y_list), zs=np.array(z_list), zdir='z', s=np.array(size), c=colour)

    ax.set_xlabel('X Label')
    ax.set_ylabel('Y Label')
    ax.set_zlabel('Z Label')

    plt.show()



################################################################

def main():
    #put here ros things and so to make IK work (super technical speech)
    rospy.init_node("rsdk_ik_service_client")
    file = '/home/student/baxter_ws/FeasibilityMatrix/fine_'
    mat = first_iteration_simplified()
    np.save((file+'matrix.npy'),mat)
    #mat = np.load((file+'matrix.npy'))
    feasible_plot(mat)
    gradients = second_iteration_simplified(mat)
    print("\n \nMatrix with feasible and unfeasible stored in: \n    "+file+'matrix.npy \n \n')
    np.save((file+'gradient.npy'),gradients)
    print("\n \nMatrix with gradient information towards feasible areas stored in: \n    "+file+'gradient.npy \n \n')
    #feasible_plot(mat)

################################################################
if __name__ == "__main__":
    main()
