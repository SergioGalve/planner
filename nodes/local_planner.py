#!/usr/bin/env python

# Imports
import rospy
import numpy as np
import copy
import baxter_interface
import baxter_external_devices
from baxter_interface import CHECK_VERSION

    # from ik_service_client
import argparse
import struct
import sys

from geometry_msgs.msg import (
    PoseStamped,
    Pose,
    Point,
    Quaternion,
)
from std_msgs.msg import (
    Header, 
    String,
)

from baxter_core_msgs.srv import (
    SolvePositionIK,
    SolvePositionIKRequest,
)

# Global declarations --> redeclared in each function as:   global xxxxxxxx
    # the point recieved from the GP
gp_msg = PoseStamped()
old_msg = PoseStamped()
current_com = dict()
next_com = dict()
first = False
fail = 0

default = PoseStamped(
                        header=Header(),
                        pose=Pose(
                            position=Point(
                                x=0.656982770038,
                                y=-0.852598021641,
                                z=0.0388609422173,
                            ),
                            orientation=Quaternion(
                                x=0.367048116303,
                                y=0.885911751787,
                                z=-0.108908281936,
                                w=0.261868353356,
                            )
                        )
                    )
# Functions


    #Callbacks
def gp_callback(msg):
    global gp_msg, first
    gp_msg = msg
    first = True

    #we do the conversion from cartesain to angular
def ik_conversion(limb="right"):
    global gp_msg, fail
    ns = "ExternalTools/"+ limb + "/PositionKinematicsNode/IKService"
    iksvc = rospy.ServiceProxy(ns,SolvePositionIK)
    ikreq = SolvePositionIKRequest()
    ikreq.pose_stamp.append(gp_msg)
    try:
        rospy.wait_for_service(ns, 5.0)
        resp = iksvc(ikreq)
    except (rospy.ServiceException, rospy.ROSException), e:
        rospy.logerr("Service call failed: %s" % (e,))
        return False

    # Check if result valid, and type of seed ultimately used to get solution
    # convert rospy's string representation of uint8[]'s to int's
    resp_seeds = struct.unpack('<%dB' % len(resp.result_type),
                               resp.result_type)
    if (resp_seeds[0] != resp.RESULT_INVALID):
        seed_str = {
                    ikreq.SEED_USER: 'User Provided Seed',
                    ikreq.SEED_CURRENT: 'Current Joint Angles',
                    ikreq.SEED_NS_MAP: 'Nullspace Setpoints',
                   }.get(resp_seeds[0], 'None')
        print("SUCCESS - Valid Joint Solution Found from Seed Type: %s" %
              (seed_str,))
        # Format solution into Limb API-compatible dictionary
        limb_joints = dict(zip(resp.joints[0].name, resp.joints[0].position))

        fail = 0
        return limb_joints
    else:
            
        fail = 1
        limb_joints = 'Error while obtaining IK'
        return limb_joints

        print("INVALID POSE - No Valid Joint Solution Found")


     # WHAT TO DO IF INVALID ?? --> since we want to have the main check in the GP, we just skip here the points that doesnt work
def check_different():
    global gp_msg
    global old_msg
    if gp_msg.pose != old_msg.pose:
        print("NEW MSG RECEIVED")
        print(" -->Old msg: ")
        print(old_msg)
        print(" -->New msg: ")
        print(gp_msg)
        old_msg = gp_msg
        return True
    else:
        return False

def check_proximity(limb,rj):
    global current_com
    threshold = 0.1  # NEED TUNING
    present = limb.joint_angles()
    #print("--Current position: ")
    #print(present)
    reach = True
    for name in rj:
        if (name in current_com) and (name in present):
            if (np.absolute(current_com[name]-present[name])>threshold):
                reach = False
        else:
            if (name in current_com):
                print("  -- The error apperar with the name " + name)
                print("Joint names not found in current_com")
                print(current_com)
            if (name in present):
                print("  -- The error apperar with the name " + name)
                print("Joint names not found in present")
                print(present)
    return reach

    # present == current_com ?? --> loop among all the names and check 


    # Loop where we set all the calls and actions to be set periodically
def loop_lp():
    pub_feedback = rospy.Publisher("LP_feedback", String, queue_size=1)
    rate = rospy.Rate(10)

    left = baxter_interface.Limb('left')
    right = baxter_interface.Limb('right')
    grip_left = baxter_interface.Gripper('left', CHECK_VERSION)
    grip_right = baxter_interface.Gripper('right', CHECK_VERSION)
    lj = left.joint_names()
    rj = right.joint_names()

    global current_com, first, next_com, fail
    n = 0
    joint_command = dict()
    while not rospy.is_shutdown():
        if bool(next_com):
            if check_different():
                joint_command = ik_conversion()
                print("--The new joint_command: ")
                print(joint_command)

            if fail == 0: # with the fail global variable we skip those cases that couldnt be translated in the IK
                if (joint_command != False) and check_proximity(right,rj): # we will need to adjust this if we want to be able to use the other arm
                    right.set_joint_positions(next_com) # we must create a dictionary --> the ik function already takes care of this !!!
                    current_com = next_com
                    next_com = joint_command
                    n+=1
                    pub_feedback.publish(('Goal #'+str(n)))
                    print(('GOAL #'+str(n)))
            else:
                n+=1
                pub_feedback.publish(('Goal #'+str(n)+' is unfeasible'))
        else:
            if first and (n == 0):
                current_com = ik_conversion()
                print("First Value Received")
                print(current_com)
                right.set_joint_positions(current_com)
                n+=1
                pub_feedback.publish(('Goal #'+str(n)))
                print(('Goal #'+str(n)))
            elif check_different():
                next_com = ik_conversion()
                print("Second Value received")
                print(next_com)



        rate.sleep()

        #rospy.spinOnce() # to obtain the feedback of the callbacks --> Necesary ?
        
# Main: Declaration 

def main():
# we must initialitate the robot and the LP node to stablish communication with the other elements
    epilog = """
See help inside the example with the '?' key for key bindings.
    """
    arg_fmt = argparse.RawDescriptionHelpFormatter
    parser = argparse.ArgumentParser(formatter_class=arg_fmt,
                                     description=main.__doc__,
                                     epilog=epilog)
    parser.parse_args(rospy.myargv()[1:])

    print("Initializing node... ")
    rospy.init_node("local_planner")
    print("Getting robot state... ")
    rs = baxter_interface.RobotEnable(CHECK_VERSION)
    init_state = rs.state().enabled
    print("NODE INITIALISED: LOCAL PLANNER ACTIVATED")

    def clean_shutdown():
        print("\nExiting example...")
        if not init_state:
            print("Disabling robot...")
            rs.disable()
    rospy.on_shutdown(clean_shutdown)

    print("Enabling robot... ")
    rs.enable()

    # Publishers and Subscribers declared before the loop starts
    rospy.Subscriber("GP_cartesian_command", PoseStamped, gp_callback)

    loop_lp()
    print("Done.")


if __name__ == '__main__':
    main()