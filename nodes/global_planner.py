#!/usr/bin/env python

# Imports
import rospy
import transforms3d 
import pydmps
import sys
import numpy as np
import math
import argparse
import copy
from planner.Trajectories import Trajectory
from geometry_msgs.msg import (
    PoseStamped,
    Pose,
    Point,
    Quaternion,
)
from std_msgs.msg import (
    Header, 
    String,
    Float64
)

#Global declarations
eeg_received = 0 #we load values and we store them in a state that has a anti wind up limitation 
eeg_state = 0
lp_feedback = 'The current header frame_id of the LP goal, so when it is achieved it uses the one in the buffer and the id changes'
last_lp = lp_feedback

xl = [0, 0]
yl = [0, 0]
zl = [0, 0]
al = [0, 0]
bl = [0, 0]
gl = [0, 0]
# number of elements per dimension
xn = 0
yn = 0
zn = 0
an = 0 # 10 between points 
bn = 0
gn = 0

ladder = np.array(([0, 0])) 


#Functions
    #Callbacks
def eeg_callback(msg):
    global eeg_received
    eeg_received = msg

def lp_callback(msg):
    global lp_feedback
    lp_feedback = msg


    #eeg state update
def eeg_update():
    windup = 1
    reduction = 0.1
    global eeg_received
    global eeg_state

    eeg_state += eeg_received*reduction
    if eeg_state>windup:
        eeg_state=windup
    if eeg_state<(-windup):
        eeg_state=-windup

    # we will test if the LP has detect that a new goal has been reached, and therefore 
def reached():
    global lp_feedback
    global last_lp
    if last_lp == lp_feedback:
        return False
    else:
        last_lp = lp_feedback
        return True

def init_board():
    global xl,yl,zl,al,bl,gl,xn,yn,zn,an,bn,gn, total_points, ladders

    # snake variables
    # limits --> based roughly on the baxter limits from the web (http://sdk.rethinkrobotics.com/wiki/Workspace_Guidelines)
    xl = [-0.5, 1.5]
    yl = [-1.5, 1,5]
    zl = [-0.5, 1.5]
    al = [0, (350*np.pi)/180]
    bl = [0, (350*np.pi)/180]
    gl = [0, (350*np.pi)/180]
    # number of elements per dimension
    xn = 21
    yn = 31
    zn = 21
    an = 36 # 10 between points 
    bn = 36
    gn = 36 

    ladders = np.load('/home/student/baxter_ws/FeasibilityMatrix/ra_gradient.npy')

def chech_ladder():
    global xl,yl,zl,al,bl,gl,xn,yn,zn,an,bn,gn, total_points,ladders

    #given a certain pose as input we find the closest one in the grid

    #we evaluate if the position given is feasible acording to the ladders matrix and if not we obtain the closest feasible one

# MAIN
# We need to create the two options where we keep the needed values loaded in the loop and we publish constantly
def main():
    global eeg_state

    input1 = sys.argv[1]
    parser = argparse.ArgumentParser(description='Choose modification approach.')
    parser.add_argument("option", help="Select a method to adapt the trajectory: force or dmp")
    args = parser.parse_args()

    rospy.init_node("global_planner")
    # Subscribers and Publishers
    pub = rospy.Publisher("GP_cartesian_command", PoseStamped, queue_size=1)
    rospy.Subscriber("EEG_signal", Float64, eeg_callback)
    rospy.Subscriber("LP_feedback", String, lp_callback)
    rate = rospy.Rate(10)
    print("NODE INITIALISED: GLOBAL PLANNER ACTIVATED")

    #some values like the dir or the parts can be sended through the parser
    direc='/home/student/baxter_ws/Trajectories/Test2/part'
    parts=1
    dim=6 # always 6 dimensions, the position must be inequivoque

    if args.option == "force":
        # we work with force system
        print("Force Mode")
        from planner.force_mod import (init_arr,load_Tr, modify_force) 
        # Load and initialize
        Tr = load_Tr(parts,direc,dim)
        Mod_Tr = init_arr(Tr,dim,parts)
        original_tr = copy.deepcopy(Mod_Tr)

        #we initialize useful variables for tracking and visualization
        i = 1
        part_size = []
        state = 0  # in which part are we
        if dim>1:
            path = Mod_Tr[state][:, 0]
            pos = Mod_Tr[state][:, 0]
            orig = original_tr[state][:, 0]

            next_goal = Mod_Tr[state][:, 0]
            quat = transforms3d.euler.euler2quat(next_goal[3],next_goal[4],next_goal[5]) 
            text = 'part: ' + str(state) + '| point: ' + str(i)
            hdr = Header(stamp=rospy.Time.now(), frame_id=text)
            print(text)

            next_pose = PoseStamped()
            next_pose.header = hdr
            next_pose.pose.position.x = next_goal[0]
            next_pose.pose.position.y = next_goal[1]
            next_pose.pose.position.z = next_goal[2]
            next_pose.pose.orientation.x = quat[1]
            next_pose.pose.orientation.y = quat[2]
            next_pose.pose.orientation.z = quat[3]
            next_pose.pose.orientation.w = quat[0]

            for i in range(parts):
                part_size.append(Mod_Tr[i].shape[1])
        else:
            path = Mod_Tr[state][0]
            pos = Mod_Tr[state][0]
            orig = original_tr[state][0]
            for i in range(parts):
                part_size.append(Mod_Tr[i].shape[0])

        n = -1
        i = 0
        count = 0
        type_force = 'Lorentz'
        # rep defines a direction is space and the modifications in the trajectory will be force towards this direction
        rep = np.array([-0.05, -0.2, -0.05, -0.0, -0.0, -0.0]) 
        next_pose = PoseStamped()
        while (not rospy.is_shutdown()) and (state < parts):
            if reached() or n<1:

                n += 1
                print(("Goal sended #"+str(n)))
                if dim > 1:
                    pos = Mod_Tr[state][:,i]
                    orig = original_tr[state][:,i]
                    next_goal=modify_force(dim,eeg_received,rep,pos,orig,False,type_force)
                    path = np.hstack((path, next_goal)) #we need to TUNE modify function !!!!!  -->  ran, len ...


                else:
                    pos = Mod_Tr[state][i]
                    orig = original_tr[state][i]
                    next_goal = modify_force(dim,eeg_received,rep,pos,orig,False,type_force)
                    path = np.hstack((path, next_goal))

                # convert euler to quat --> http://matthew-brett.github.io/transforms3d/
                quat = transforms3d.euler.euler2quat(next_goal[3],next_goal[4],next_goal[5]) 
                text = 'part: ' + str(state) + '| point: ' + str(i)
                hdr = Header(stamp=rospy.Time.now(), frame_id='base')

                next_pose.header = hdr
                next_pose.pose.position.x = next_goal[0]
                next_pose.pose.position.y = next_goal[1]
                next_pose.pose.position.z = next_goal[2]
                next_pose.pose.orientation.x = 0.367048116303
                next_pose.pose.orientation.y = 0.885911751787
                next_pose.pose.orientation.z = -0.108908281936
                next_pose.pose.orientation.w = 0.261868353356
                #next_pose.pose.orientation.x = quat[1]
                #next_pose.pose.orientation.y = quat[2]
                #next_pose.pose.orientation.z = quat[3]
                #next_pose.pose.orientation.w = quat[0]
                # we should create a TOPIC to confirm that has been received !!!!
                i += 1
                count += 1
                if i == part_size[state]:
                    i = 0
                    state += 1
                if(state == parts):
                    #the final visualization of possible plots can be coded here
                    print("END OF TRAJECTORY - GP DISABLED")
            pub.publish(next_pose)
            rate.sleep()


    elif args.option == "dmp":
        #we will use dmps
        print("DMP Mode")
        from planner.dmp_mod import (load_Tr, init_dmp, modify_dmp) 
        # Load and initialize
        Tr = load_Tr(parts,direc,dim)
        Dmps, Mod_Tr = init_dmp(Tr,dim,parts,False)
        original_tr = copy.deepcopy(Mod_Tr)
        original_dmp = copy.deepcopy(Dmps)

        i = 1
        part_size = []
        state = 0  # in which part are we
        if dim>1:
            path = Mod_Tr[state][:, 0]
            next_goal = Mod_Tr[state][:, 0]
            quat = transforms3d.euler.euler2quat(next_goal[3],next_goal[4],next_goal[5]) 
            text = 'part ' + str(state) + '| point ' + str(i)
            hdr = Header(stamp=rospy.Time.now(), frame_id=text)
            print(text)

            next_pose = PoseStamped()
            next_pose.header = hdr
            next_pose.pose.position.x = next_goal[0]
            next_pose.pose.position.y = next_goal[1]
            next_pose.pose.position.z = next_goal[2]
            next_pose.pose.orientation.x = quat[1]
            next_pose.pose.orientation.y = quat[2]
            next_pose.pose.orientation.z = quat[3]
            next_pose.pose.orientation.w = quat[0]

            for i in range(parts):
                part_size.append(Mod_Tr[i].shape[1])

        else:
            path = Mod_Tr[state][0]
            for i in range(parts):
                part_size.append(Mod_Tr[i].shape[0])

        n = -1
        i = 1
        count = 0
        vect = [0.2, 0.1, 0.5, 0.0, 0.0, 0.0]
        
        while (state < parts) and (not rospy.is_shutdown()):
            if reached() or n<1:
                n += 1
                 # eeg_state must be multidimensional (pregiven direction (in x only, in the opposite direction of the user...)) --> only one DoF !!!!!
                Dmps, Mod_Tr = modify_dmp(state,Dmps,eeg_received*vect,parts,Mod_Tr,part_size,original_dmp,False) # eeg_state ??????
                if dim > 1:
                    next_goal = Mod_Tr[state][:,i]
                    path = np.hstack((path,next_goal))
                else:
                    next_goal = Mod_Tr[state][i]
                    path = np.hstack((path, next_goal))

                # convert euler to quat --> http://matthew-brett.github.io/transforms3d/
                quat = transforms3d.euler.euler2quat(next_goal[3],next_goal[4],next_goal[5]) 
                text = 'part: ' + str(state) + '| point: ' + str(i)
                hdr = Header(stamp=rospy.Time.now(), frame_id=text)
                print(text)

                next_pose.header = hdr
                next_pose.pose.position.x = next_goal[0]
                next_pose.pose.position.y = next_goal[1]
                next_pose.pose.position.z = next_goal[2]
                next_pose.pose.orientation.x = quat[1]
                next_pose.pose.orientation.y = quat[2]
                next_pose.pose.orientation.z = quat[3]
                next_pose.pose.orientation.w = quat[0]
                
                i += 1
                count += 1
                if i == part_size[state]:
                    i = 0
                    state += 1
                if(state == parts):
                    #the final visualization of possible plots can be coded here
                    print("END OF TRAJECTORY - GP DISABLED")
            pub.publish(next_pose)
            rate.sleep()

    else:
        print("Choose one of the implemented options:\n  - force\n  - dmp")

################################################################
if __name__ == "__main__":
    main()