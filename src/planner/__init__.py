from .dmp_mod import (load_Tr, init_dmp, modify_dmp) 
from .force_mod import (init_arr,load_Tr, modify_force) 
from .Trajectories import Trajectory
